title: Welcome
description: MkDocs template

# Welcome

![graduation hat](assets/images/school160.png)

## The Goal

Describe here what your intent is with these pages. Why do you need these pages? What is the problem you are trying to solve.

Address the right audience, who should read this information?

## Maintainer info

The ["Maintainer info"](maintainer-info/introduction.md) section explains the different aspects of this site. They can help set things upinitially or for reference in a later stage.