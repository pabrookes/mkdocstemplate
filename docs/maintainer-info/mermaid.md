# Mermaid

Mermaid is enabled through the superfences extension. An online connection is required.

```nohighlight
'''mermaid
graph TD;
A --> B;
A --> C;
B --> D;
C --> D;
'''
```

_Don't forget to use three backticks around your code block._

The above text converts to the following diagram

```mermaid
graph TD;
A --> B;
A --> C;
B --> D;
C --> D;
```

Thanks to @Taka710's post on [Qiitta.com](https://qiita.com/Taka710/items/3e4a01fcc1d907967cb2)
Although most of the text was unreadable (for me :wink:), it pointed me in the right direction.
