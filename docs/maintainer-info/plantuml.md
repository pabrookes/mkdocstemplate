# PlantUML

The PlantUML diagram is written in text but converted into an image on build.

```nohighlight
'''plantuml format="svg" classes="uml myDiagram" width="300px"
skinparam monochrome true
skinparam ranksep 20
skinparam dpi 150
skinparam arrowThickness 0.7
skinparam packageTitleAlignment left
skinparam usecaseBorderThickness 0.4
skinparam defaultFontSize 12
skinparam rectangleBorderThickness 1

rectangle "Main" {
  (main.view)
  (singleton)
}
rectangle "Base" {
  (base.component)
  (component)
  (model)
}
rectangle "<b>main.ts</b>" as main_ts

(component) ..> (base.component)
main_ts ==> (main.view)
(main.view) --> (component)
(main.view) ...> (singleton)
(singleton) ---> (model)
'''
```

_Don't forget to use three backticks around your code block._

The text above converts to the following diagram:

```plantuml format="svg" classes="uml myDiagram" width="300px"
skinparam monochrome true
skinparam ranksep 20
skinparam dpi 150
skinparam arrowThickness 0.7
skinparam packageTitleAlignment left
skinparam usecaseBorderThickness 0.4
skinparam defaultFontSize 12
skinparam rectangleBorderThickness 1

rectangle "Main" {
  (main.view)
  (singleton)
}
rectangle "Base" {
  (base.component)
  (component)
  (model)
}
rectangle "<b>main.ts</b>" as main_ts

(component) ..> (base.component)
main_ts ==> (main.view)
(main.view) --> (component)
(main.view) ...> (singleton)
(singleton) ---> (model)
```

## Example swimming lanes diagram

A larger swimming lanes example:

```nohighlight
'''plantuml format="svg" classes="uml myDiagram" alt="My swimming lanes PlantUML example" title="My swimming lanes PlantUML example" width="1400px"

skinparam monochrome true

title Developer portal, Drupal & Content update flows

box "External"
  actor "Drupal developer" as ddev order 10
  database "CodeRepo" as cr order 20
end box

actor "SysAdmin" as admin order 25
control "CI/CD" as cicd order 30
participant DEV as dev order 40
participant STG as stg order 50
participant PRD as prd order 60
participant VPC as vpc order 65

box "API Team"
actor "Content editor" as ced order 70
actor "API developer" as adev order 80
end box

ced ->> prd : content
ced -->> admin : request content update as part of the devportal update

== PREPARE CONTENT UPDATE (optional step) ==

admin ->> cicd : ContentSync_Build
activate cicd
  cicd -> prd : request content
  prd -> cicd : download content
  cicd -> cicd : set up workspace
  cicd -> admin :
deactivate cicd

== CONTENT UPDATE DEVPORTAL (optional step) ==

...  CONTENT UPDATE DEVPORTAL in DEV (optional step) ...

admin ->> cicd : Sync_PRD_to_Dev
activate cicd
  cicd -> dev : content push
cicd -> admin :
deactivate cicd

... CONTENT UPDATE DEVPORTAL in STG (optional step) ...

admin ->> cicd : Sync_PRD_to_Stg
activate cicd
  cicd -> stg : content push
cicd -> admin :
deactivate cicd

... ...

ddev -> cr : drupal code
ddev -->> adev : drupal code ready
adev -->> admin : request drupal update (coderepo branch provided in request)

== PREPARE DRUPAL UPDATE DEVPORTAL ==

admin ->> cicd : configure Devportal_build
activate cicd
  cicd ->> cicd : edit coderepo branch
cicd -> admin :
deactivate cicd

admin ->> cicd : run Devportal_build
activate cicd
  cicd -> cr : request code
  cr -> cicd : download code
  cicd -> cicd : set up workspace
cicd -> admin :
deactivate cicd

... ...

== DRUPAL UPDATE DEVPORTAL ==

... DRUPAL UPDATE DEVPORTAL in DEV ...

admin ->> cicd : Devportal_deploy_DEV_V1-default
activate cicd
  cicd -> dev : upload code
cicd -> admin :
deactivate cicd

... DRUPAL UPDATE DEVPORTAL in STG ...

admin ->> cicd : Devportal_deploy_STG_V1-default
activate cicd
  cicd -> stg : upload code
cicd -> admin :
deactivate cicd

... CREATE SNAPSHOT of DEVPORTAL in PRD ...

admin ->> vpc : manual step: submit snapshot request in vRA
vpc -> admin : request submit ok
admin ->> vpc : manual step: verify snapshot request in vRA
vpc -> admin : snapshot verified

... DRUPAL UPDATE DEVPORTAL in PRD ...

admin ->> cicd : Devportal_deploy_PROD_V1-default
activate cicd
  cicd -> prd : upload code
cicd -> admin :
deactivate cicd
'''
```

And the actual image:

```plantuml format="svg" classes="uml myDiagram" alt="My swimming lanes PlantUML example" title="My swimming lanes PlantUML example" width="1400px"

skinparam monochrome true

title Developer portal, Drupal & Content update flows

box "External"
  actor "Drupal developer" as ddev order 10
  database "CodeRepo" as cr order 20
end box

actor "SysAdmin" as admin order 25
control "CI/CD" as cicd order 30
participant DEV as dev order 40
participant STG as stg order 50
participant PRD as prd order 60
participant VPC as vpc order 65

box "API Team"
actor "Content editor" as ced order 70
actor "API developer" as adev order 80
end box

ced ->> prd : content
ced -->> admin : request content update as part of the devportal update

== PREPARE CONTENT UPDATE (optional step) ==

admin ->> cicd : ContentSync_Build
activate cicd
  cicd -> prd : request content
  prd -> cicd : download content
  cicd -> cicd : set up workspace
  cicd -> admin :
deactivate cicd

== CONTENT UPDATE DEVPORTAL (optional step) ==

...  CONTENT UPDATE DEVPORTAL in DEV (optional step) ...

admin ->> cicd : Sync_PRD_to_Dev
activate cicd
  cicd -> dev : content push
cicd -> admin :
deactivate cicd

... CONTENT UPDATE DEVPORTAL in STG (optional step) ...

admin ->> cicd : Sync_PRD_to_Stg
activate cicd
  cicd -> stg : content push
cicd -> admin :
deactivate cicd

... ...

ddev -> cr : drupal code
ddev -->> adev : drupal code ready
adev -->> admin : request drupal update (coderepo branch provided in request)

== PREPARE DRUPAL UPDATE DEVPORTAL ==

admin ->> cicd : configure Devportal_build
activate cicd
  cicd ->> cicd : edit coderepo branch
cicd -> admin :
deactivate cicd

admin ->> cicd : run Devportal_build
activate cicd
  cicd -> cr : request code
  cr -> cicd : download code
  cicd -> cicd : set up workspace
cicd -> admin :
deactivate cicd

... ...

== DRUPAL UPDATE DEVPORTAL ==

... DRUPAL UPDATE DEVPORTAL in DEV ...

admin ->> cicd : Devportal_deploy_DEV_V1-default
activate cicd
  cicd -> dev : upload code
cicd -> admin :
deactivate cicd

... DRUPAL UPDATE DEVPORTAL in STG ...

admin ->> cicd : Devportal_deploy_STG_V1-default
activate cicd
  cicd -> stg : upload code
cicd -> admin :
deactivate cicd

... CREATE SNAPSHOT of DEVPORTAL in PRD ...

admin ->> vpc : manual step: submit snapshot request in vRA
vpc -> admin : request submit ok
admin ->> vpc : manual step: verify snapshot request in vRA
vpc -> admin : snapshot verified

... DRUPAL UPDATE DEVPORTAL in PRD ...

admin ->> cicd : Devportal_deploy_PROD_V1-default
activate cicd
  cicd -> prd : upload code
cicd -> admin :
deactivate cicd
```

## Credits

Thanks to Michelle Tessaro's (mikitex70) [PlantUML plugin for Python-Markdown](https://github.com/mikitex70/plantuml-markdown) we can now use PlantUML within MkDocs.

Another interesting alternative for displaying diagrams of all sorts is [Kroki](https://kroki.io/). I'm not sure how to use it yet but it's an interesting one stop shop for all your diagrams.

:bulb: Maybe an idea to create a VS Code plugin based on Kroki.
